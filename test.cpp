#include <igloo/igloo.h>
#include "functions.h"

using namespace igloo;


Context(TestLetterGradeFunction) {
    Spec(Convert94ToA){
        Assert::That(numberToLetter(94), Equals("A"));
    }
    Spec(Convert87ToB){
        Assert::That(numberToLetter(87), Equals("B"));
    }
    Spec(Convert75ToC){
        Assert::That(numberToLetter(75), Equals("C"));
    }
    Spec(Convert63ToD){
        Assert::That(numberToLetter(63), Equals("D"));
    }
    Spec(Convert51ToF){
        Assert::That(numberToLetter(51), Equals("F"));
    }
    Spec(Convert19ToF){
        Assert::That(numberToLetter(19), Equals("F"));
    }
    Spec(Convert101ToInvalid){
        Assert::That(numberToLetter(101), Equals("Invalid grade"));
    }
    Spec(ConvertNegative7ToInvalid){
        Assert::That(numberToLetter(-7), Equals("Invalid grade"));
    }
};


int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}





