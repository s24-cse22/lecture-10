#include <iostream>
#include "functions.h"

using namespace std;


int main(){

    // Your code here

    string name;
    getline(cin, name);
    string subject1;
    getline(cin, subject1);
    string subject2;
    getline(cin, subject2);
    string subject3;
    getline(cin, subject3);

    int grade1;
    cin >> grade1;

    int grade2;
    cin >> grade2;

    int grade3;
    cin >> grade3;

    string letter1 = numberToLetter(grade1);
    string letter2 = numberToLetter(grade2);
    string letter3 = numberToLetter(grade3);

    cout << name << endl;
    cout << subject1 << "\t" << letter1 << endl;
    cout << subject2 << "\t" << letter2 << endl;
    cout << subject3 << "\t" << letter3 << endl;


    return 0;
}