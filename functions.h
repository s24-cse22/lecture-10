#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <iostream>
// Functions go here

std::string numberToLetter(int numerical){
    if (numerical > 100 || numerical < 0){
        return "Invalid grade";
    }

    if (numerical >= 90){
        return "A";
    }
    else if(numerical >= 80){
        return "B";
    }
    else if(numerical >= 70){
        return "C";
    }
    else if (numerical >= 60){
       return "D";
    }
    else{
        return "F";
    }
}


#endif